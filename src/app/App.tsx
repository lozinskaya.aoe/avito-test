import React, { useEffect } from 'react';
import { Modal } from '../component/modal/modal';
import { PhotoList } from '../component/photoList/photoList';
import { fetchPhotoCollection,  store, useDispatch } from '../store';
import { TypeContext } from '../type';
import styles from './app.module.css';

export const StoreContext = React.createContext<TypeContext>(store);

export function App() {
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(fetchPhotoCollection());
  }, [])

  return (
    <div className={styles.add}>
      <h1>TEST APP</h1>

      <StoreContext.Provider value={store}>  
        <Modal/>
        <PhotoList/>
      </StoreContext.Provider>

    </div>
  );
}
