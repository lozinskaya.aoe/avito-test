import { clearFormComment, closeClickedPhoto, createComment, downloadPhoto, downloadPhotoModal, openClickedPhoto } from "./store";

export type ClickedPhoto = {
    id: number,
    url: string
};

export type PhotoCollection = ClickedPhoto[];

export type EnteredComment = {
    photoId: number,
    comment: {
        name: string,
        comment: string
    }
}

export type PhotoModal = {
    id: number,
    url: string,
    comments: Comments[]
}

export type Action = ReturnType<typeof openClickedPhoto>
    | ReturnType<typeof downloadPhoto>
    | ReturnType<typeof downloadPhotoModal>
    | ReturnType<typeof closeClickedPhoto>
    | ReturnType<typeof createComment>
    | ReturnType<typeof clearFormComment>



export type Selector = (state: CurrentState) => CurrentState;
export type Reducer = (state: CurrentState, action: Action) => CurrentState
export type Dispatch = (action: Action | ((dispatch: Dispatch, getState: () => CurrentState) => void)) => void
export type Listener = (state: CurrentState) => void;

export type TypeContext = {
    getState: () => CurrentState,
    dispatch: Dispatch,
    subscribe: (listener: Listener) => VoidFunction,
}

export type CurrentState = {
    photoCollection: PhotoCollection,
    clickedPhoto: ClickedPhoto | null,
    photoModal: PhotoModal | null,
    enteredComment: EnteredComment | null
}

export type Comments = {
    id: number,
    text: string,
    date: number
}



