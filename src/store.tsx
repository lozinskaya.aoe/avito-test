import React, { useContext, useEffect, useState } from "react";
import { getPhotoModal, getPhotoList, postCommentToPhoto } from "./api";
import { StoreContext } from "./app/App";
import { CurrentState, Dispatch, Listener, PhotoModal, PhotoCollection, Reducer, TypeContext, Action, ClickedPhoto, EnteredComment } from "./type";



export const createStore = function (reducer: Reducer, initialState: CurrentState): TypeContext {
    let currentState: CurrentState = initialState;
    const listeners: Listener[] = [];

    const getState = () => {
        return currentState
    };

    const dispatch: Dispatch = (action) => {
        if (typeof action === "function") {
            action(dispatch, getState);
        } else {
            currentState = reducer(currentState, action);
            listeners.forEach(listener => { 
                console.log(listener);
                listener(currentState)});
        }
    }

    const subscribe = (listener: Listener) => {
        listeners.push(listener);
        return () => listeners.splice(listeners.indexOf(listener), 1);
    };
    return { getState, dispatch, subscribe }
}


// actions

const DOWNLOAD_PHOTO = "DOWNLOAD_PHOTO";
const OPEN_CLICKED_PHOTO = "OPEN_CLICKED_PHOTO";
const CLOSE_CLICKED_PHOTO = "CLOSE_CLICKED_PHOTO";
const DOWNLOAD_PHOTO_MODAL = "DOWNLOAD_PHOTO_MODAL";
const CREATE_COMMENT = "CREATE_COMMENT";
const CLEAR_FORM_COMMENT = "CLEAR_FORM_COMMENT";


// action creators

export function downloadPhoto(value: PhotoCollection) {
    return {
        type: DOWNLOAD_PHOTO,
        photoCollection: value
    } as const
}

export function openClickedPhoto(value: ClickedPhoto) {
    return {
        type: OPEN_CLICKED_PHOTO,
        clickedPhoto: value
    } as const
}

export function closeClickedPhoto() {
    return {
        type: CLOSE_CLICKED_PHOTO,
        clickedPhoto: null,
        photoModal: null,
    }  as const
}

export function downloadPhotoModal(value: PhotoModal) {
    return {
        type: DOWNLOAD_PHOTO_MODAL,
        photoModal: value
    } as const
}

export function createComment(value: EnteredComment){
    return {
        type: CREATE_COMMENT,
        enteredComment: value
    } as const
}

export function clearFormComment(){
    return {
        type: CLEAR_FORM_COMMENT,
        enteredComment: null
    } as const
}


// fetch

export function fetchPhotoCollection() {

    return function (dispatch: Dispatch, getState: Function) {
        getPhotoList()
            .then(photoList => dispatch(downloadPhoto(photoList)))
    }
}

export function fetchPhoto() {
    return function (dispatch: Dispatch, getState: ()=> CurrentState) {
        const state = getState();
        if(state.clickedPhoto){
            getPhotoModal(state.clickedPhoto.id)
            .then(photoModal => dispatch(downloadPhotoModal(photoModal)))
        }
    }
}

export function postComment() {
    return function(dispatch:Dispatch, setState: () => CurrentState){
        const state = setState();
        if(state.enteredComment){
            postCommentToPhoto(state.enteredComment)
            .then( () => dispatch(clearFormComment()))
        }
      
    }
}

// reducer

export function reducer(currentState: CurrentState, action: Action) {
    switch (action.type) {
        case DOWNLOAD_PHOTO: {
            const newCurrentState = { ...currentState };
            newCurrentState.photoCollection = [...action.photoCollection];
            return newCurrentState;
        }
        case OPEN_CLICKED_PHOTO: {
            const newCurrentState = { ...currentState };
            newCurrentState.clickedPhoto = { ...action.clickedPhoto };
            return newCurrentState
        }
        case DOWNLOAD_PHOTO_MODAL: {
            const newCurrentState = { ...currentState };
            newCurrentState.photoModal = { ...action.photoModal };
            return newCurrentState
        }
        case CLOSE_CLICKED_PHOTO: {
            const newCurrentState = { ...currentState };
            newCurrentState.photoModal = action.photoModal;
            newCurrentState.clickedPhoto = action.clickedPhoto;
            return newCurrentState
        }
        case CREATE_COMMENT: {
            const newCurrentState = { ...currentState };
            newCurrentState.enteredComment = {...action.enteredComment};
            return newCurrentState
        }
        case CLEAR_FORM_COMMENT: {
            const newCurrentState = { ...currentState };
            newCurrentState.enteredComment = action.enteredComment;
            return newCurrentState
        }
        default:
            return currentState
    }
}


//hook

export function useSelector<TField>(selector: (state: CurrentState) => TField) {
    const context = useContext(StoreContext);
    const [state, setState] = useState(() => selector(context.getState()));

    useEffect(() => {
        const listener: Listener = (newState) => setState(selector(newState));
        const unsubscribe = context.subscribe(listener);

        return () => unsubscribe();
    }, [])

    return state
}

export function useDispatch() {
    const context = useContext(StoreContext);
    return context.dispatch
}

export const store = createStore(reducer, { photoCollection: [], clickedPhoto: null, photoModal: null, enteredComment: null });
