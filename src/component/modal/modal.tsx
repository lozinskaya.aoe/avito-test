import { useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import styles from './modal.module.css'
import closeIcon from '../../icon/outline_close_black_48dp.png';
import { formattingMillisecondInDate } from "../../helpmate/formattingMillisecondInDate";
import { closeClickedPhoto, createComment, fetchPhoto, postComment, useDispatch, useSelector } from "../../store";


const body = document.body;

export const Modal = () => {
    const [commentText, setComment] = useState('');
    const [commentatorName, setCommentatorName] = useState('');
    const modalRef = useRef<HTMLDivElement>(null);
    const dispatch = useDispatch();
    const photo = useSelector((state) => state.photoModal);
    const clickedPhoto = useSelector( state => state.clickedPhoto)

    useEffect(() => {
        if(clickedPhoto !== null){
            dispatch(fetchPhoto())
        }
    },[clickedPhoto])

    useEffect(() => {
        const closeModal = (event: any) => {
            if (modalRef.current && !modalRef.current.contains(event.target)) {
                dispatch(closeClickedPhoto());
                console.log('я тут')
            }
        }

        window.addEventListener('click', closeModal);
        console.log('подписался')
        return () => {
            console.log('отписался')
            window.removeEventListener('click', closeModal);
        }
    }, [clickedPhoto])

    useEffect(() => {
        if (!photo) {
            document.body.className = ""
        } else {
            document.body.className = styles.scrollNone;
        }
    }, [photo])

    if (!photo) {
        return null
    }


    function fetchComment() {
        if (commentatorName === '') {
            alert("Введите имя")
        }

        if (commentText === '') {
            alert("Введите текст комментария")
        }

        if (commentatorName !== '' && commentText !== '') {
            const comment = { comment: {name: commentatorName, comment: commentText}, photoId: photoId};
            dispatch(createComment(comment));
            dispatch(postComment());
            dispatch(closeClickedPhoto());
        }
    }

    const photoUrl = photo.url;
    const photoId = photo.id;

    return ReactDOM.createPortal(
        <div className={styles.blackout} >

            <div
                className={styles.modal}
                ref={modalRef}
            >
                <img
                    src={closeIcon}
                    className={styles.closeIcon}
                />

                <img
                    src={photoUrl}
                    className={styles.photo}
                />

                <div className={styles.form}>
                    <input
                        type="text"
                        name="name"
                        placeholder="Ваше имя"
                        onChange={(event) => setCommentatorName(event.target.value)}
                    />

                    <input
                        type="textarea"
                        name="comments"
                        placeholder="Ваш комментарий"
                        onChange={(event) => setComment(event.target.value)}
                    />

                    <input
                        type="button"
                        value={"Оставить комментарий"}
                        onClick={() => fetchComment()}
                    />
                </div>


                <div className={styles.comments}>
                    {photo.comments.map(comment => (
                        <div key={comment.id}>
                            <h1>
                                {formattingMillisecondInDate(comment.date)}
                            </h1>
                            <p>
                                {comment.text}
                            </p>
                        </div>)
                    )}
                </div>

            </div>
        </div>
        , body);
};