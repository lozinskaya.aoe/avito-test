import { useSelector } from '../../store';
import { ClickedPhoto } from '../../type';
import { Photo } from "../photo/photo";
import styles from './photoList.module.css'

//type Props = {  openModal: (id: number) => void }

// export const PhotoList = (props: Props) => {
//     const photoList = useSelector((state) => state.photoCollection);

//     return (
//         <div className={styles.photoList}>
//             {photoList.map( (photo:OnePhoto ) => <Photo
//                 key={photo.id}
//                 id={photo.id}
//                 url={photo.url}
//                 openModal={props.openModal}
//             />)}
//         </div>
//     )
// }

export const PhotoList = () => {
    const photoList = useSelector((state) => state.photoCollection);

    return (
        <div className={styles.photoList}>
            {photoList.map( (photo:ClickedPhoto ) => <Photo
                key={photo.id}
                id={photo.id}
                url={photo.url}
            />)}
        </div>
    )
}