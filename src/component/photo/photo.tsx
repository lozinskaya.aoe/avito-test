import { openClickedPhoto, useDispatch } from '../../store';
import styles from './photo.module.css'



type Props = { id: number, url: string }

export const Photo = (props: Props) => {
    const url = props.url;
    const id = props.id
    const dispatch = useDispatch();

    return <img
        className={styles.photo}
        src={url}
        onClick={() => dispatch(openClickedPhoto({ id: id, url: url }))}
    />

}