import { EnteredComment, PhotoCollection } from './type';


const photoList = "https://boiling-refuge-66454.herokuapp.com/images";
const photoAndCommints = "https://boiling-refuge-66454.herokuapp.com/images/";



export function getPhotoList(): Promise<PhotoCollection> {
    const promise = fetch(photoList).
        then(result => result.json());
    return promise;
}

export function getPhotoModal(photoId: number) {
    const promise = fetch(photoAndCommints + photoId).then(resulte => resulte.json());
    return promise;
}


export function postCommentToPhoto({photoId, comment, }: EnteredComment) {
    const promise = fetch(photoAndCommints + photoId + "/comments", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
        body: JSON.stringify(comment)
    })
        .then(response => {
            if (response.ok) {
                alert('Комментарий успешно отправлен')
            } 
        })
    return promise;
}